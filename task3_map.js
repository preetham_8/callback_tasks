function callbackParent(arr, cb) {
 for (var index = 0; index < arr.length; index++) {
  cb(arr[index], index, arr);
 }
 }
 var arr = [1, 2, 3];
 callbackParent(arr, function (element, index, self) {
 console.log("custom callback element", element);
 console.log("index", index);
 console.log("self", self);
 });
